<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

use App\Ctrl\Front;
use App\Ctrl\Sanityze;

require __DIR__ . '/../../vendor/autoload.php';

// Instantiate App
$app = AppFactory::create();

// Add error middleware
$app->addErrorMiddleware(true, true, true);

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../templates/');
$twig = new Twig\Environment($loader, [
    'debug' => true,
    'cache' => false,
]);

$twig->addExtension(new \Twig\Extension\DebugExtension());

$security = new Sanityze("../config/security.yaml");


// Add routes
$app->get('/', function (Request $request, Response $response) {
  $page = new Front("getForm", $request, $response);
  return $page->response;
});
$app->post('/', function (Request $request, Response $response) {
    $page = new Front("postForm", $request, $response);
    return $page->response;
});

$app->get('/hello/{name}', function (Request $request, Response $response, $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name");
    return $response;
});

$app->run();