<?php

namespace App\Ctrl;

use App\Ctrl\Page;

class Front extends Page{
  public function getForm(){
    $this->template = "formulaire";
  }
  public function postForm($request){
    global $security;
    $data                  = $security->post($request->getParsedBody());
    $data["usernameError"] = $security->isValid("username");
    $data["emailError"]    = $security->hasBeenModified("email");
    $data["messageError"]  = $security->isValid("message");
    $this->template        = "formulaire";
    $this->data = $data;
  }
}